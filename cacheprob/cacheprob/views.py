from django.views.generic import TemplateView
from django.http import HttpResponse
from django.shortcuts import render
from django.core.cache import cache
import random
import json


class MainView(TemplateView):
    template_name = "main.html"

    def get(self, request):
        return render(request, self.template_name, {})

    def post(self, request):
        data = {}
        county_key = self._convert_county_to_key(request.POST.get('county', None))
        frl = self.first_rate_lookup(county_key)
        if frl:
            data = {
                'tax_rate': frl,
                'function_used': 'first_rate_lookup',
                'key': county_key
            }

        else:
            data = {
                'tax_rate': self.sales_tax_lookup(county_key),
                'function_used': 'sales_tax_lookup',
                'key': county_key
            }
        return HttpResponse(json.dumps(data), content_type="application/json")

    def _convert_county_to_key(self, county):
        if not county:
            return False
        return str(county).replace(" ", "_")

    def first_rate_lookup(self, key):
        print "performing first_rate_lookup"
        return cache.get(key, False)

    def sales_tax_lookup(self, key):
        """ Sets in cache and returns a random sales tax rate for a county """
        print "performing sales_tax_lookup"
        tax_percent = "%s%%" % random.choice(range(0, 14))
        cache.set(key, tax_percent, None)
        return tax_percent
